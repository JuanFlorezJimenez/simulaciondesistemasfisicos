double CeroPorBiseccion(double a, double b,double alpha)
{
  double fa=Bessel(alpha,a),fb=Bessel(alpha,b);
  double m,fm;
  double Err=1e-7;
  
  while((b-a)>Err)
    {
      m=(a+b)/2;
      fm=Bessel(alpha,m);
      if(fb*fm<=0)
	{a=m;
	  fa=fm;}
      else
	{b=m;
	  fb=fm;}
      
    }

      return (a+b)/2;

}
